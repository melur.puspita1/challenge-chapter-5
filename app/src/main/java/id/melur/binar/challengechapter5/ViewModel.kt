package id.melur.binar.challengechapter5

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.Navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter5.database.User
import id.melur.binar.challengechapter5.database.UserDatabase
import id.melur.binar.challengechapter5.helper.UserRepo
import id.melur.binar.challengechapter5.model.MoviePopular
import id.melur.binar.challengechapter5.model.MoviePopularItem
import id.melur.binar.challengechapter5.model.MovieResponse
import id.melur.binar.challengechapter5.service.TMDBApiService
import id.melur.binar.challengechapter5.service.TMDBClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewModel(private val userRepo: UserRepo, private val apiService: TMDBApiService) : ViewModel() {

    // di gunakan untuk assign value, hanya untuk di dalam view model
    private val _dataSuccess = MutableLiveData<MovieResponse>()

    // di gunakan untuk observe di fragment/activity
    val dataSuccess: LiveData<MovieResponse> get() = _dataSuccess


    private val _detailSuccess = MutableLiveData<MoviePopularItem>()
    val detailSuccess: LiveData<MoviePopularItem> get() = _detailSuccess

    private val _dataError = MutableLiveData<String>()
    val dataError: LiveData<String> get() = _dataError

    val user = MutableLiveData<List<User>>()
    val addDataUser = MutableLiveData<Boolean>()
    val isLogin = MutableLiveData<Boolean>()
    val usernamee = MutableLiveData<String>()
    val name = MutableLiveData<String>()
    val dateOfBirth = MutableLiveData<String>()
    val address = MutableLiveData<String>()


    // Get data dari database room
    fun getData() {
        // viewmodel scope penggunaannya mirip coroutinescope
        viewModelScope.launch {
            user.value = userRepo.getDataUser()
        }
    }

    fun checkRegisteredUser(username: String, password: String) {
        viewModelScope.launch {
            user.value = userRepo.getRegisteredUser(username, password)
            if (!user.value.isNullOrEmpty()) {
                isLogin.value = true
                usernamee.value = username
            }
        }
    }

    fun addDataUser(username: String, email: String, password: String) {
        viewModelScope.launch {
            val user = User(null, username, email, password,"","","")
            userRepo.insertUser(user)
            addDataUser.value = true
        }
    }

    fun updateUser(username: String, name: String, dateOfBirth: String, address: String) {
        viewModelScope.launch {
            userRepo.updateUser(username, name, dateOfBirth, address)
        }
    }

    fun getUser(username: String) {
        viewModelScope.launch {
            userRepo.getUser(username)
        }
    }

    fun coba(username: String) {
        viewModelScope.launch {
            val result = userRepo.coba(username)
            val test = result!!.elementAt(0)
            val etName = test.name
            val etDate = test.dateOfBirth
            val etAddress = test.address
            name.postValue(etName)
            dateOfBirth.postValue(etDate)
            address.postValue(etAddress)
        }
    }

    fun getDetailMovie(movieId: Int) {
        apiService.getDetailMovie(movieId = movieId, BuildConfig.API_KEY)
            .enqueue(object : Callback<MoviePopularItem> {
                override fun onResponse(
                    call: Call<MoviePopularItem>,
                    response: Response<MoviePopularItem>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _detailSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("Datanya kosong")
                        }
                    } else {
                        _dataError.postValue("Pengambilan data gagal}")
                    }
                }

                override fun onFailure(call: Call<MoviePopularItem>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                }
            })
    }

}
