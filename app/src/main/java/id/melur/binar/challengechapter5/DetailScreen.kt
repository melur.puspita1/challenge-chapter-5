package id.melur.binar.challengechapter5

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import id.melur.binar.challengechapter5.databinding.FragmentDetailScreenBinding
import id.melur.binar.challengechapter5.databinding.FragmentSplashScreenBinding
import id.melur.binar.challengechapter5.helper.UserRepo
import id.melur.binar.challengechapter5.helper.viewModelsFactory
import id.melur.binar.challengechapter5.model.MoviePopularItem
import id.melur.binar.challengechapter5.service.TMDBApiService
import id.melur.binar.challengechapter5.service.TMDBClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailScreen : Fragment() {

    private var _binding: FragmentDetailScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences
    private var dataUsername: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val apiService : TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, apiService) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDetailScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val movieId = arguments?.getInt("id")
        viewModel.getDetailMovie(movieId!!)
        observeData()
    }

    private fun observeData() {
        viewModel.detailSuccess.observe(viewLifecycleOwner) {
            binding.apply {
                Glide.with(requireContext())
                    .load("https://www.themoviedb.org/t/p/w1920_and_h800_multi_faces/" + it.backdropPath)
                    .into(ivBackdrop)
                Glide.with(requireContext())
                    .load(BuildConfig.BASE_URL_IMAGE + it.posterPath)
                    .into(ivPoster)
                tvTitle.text = it.title
                tvOverview.text = it.overview
                tvDate.text = it.releaseDate
            }
        }
    }

}