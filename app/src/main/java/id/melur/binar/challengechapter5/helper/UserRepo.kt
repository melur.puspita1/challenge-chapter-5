package id.melur.binar.challengechapter5.helper

import android.content.Context
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter5.database.User
import id.melur.binar.challengechapter5.database.UserDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserRepo(context: Context) {

    private val mDb = UserDatabase.getInstance(context)

    suspend fun getDataUser() = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getAllUser()
    }

    suspend fun updateUser(username: String, name: String, dateOfBirth: String, address: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.updateUser(username, name, dateOfBirth, address)
    }

    suspend fun insertUser(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.insertUser(user)
    }

    suspend fun deleteUser(user: User) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.deleteUser(user)
    }

    suspend fun getRegisteredUser(username: String, password: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getRegisteredUser(username, password)
    }

    suspend fun getUser(username: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUser(username)
    }

    suspend fun coba(username: String) = withContext(Dispatchers.IO) {
        mDb?.userDao()?.getUser(username)
    }
}