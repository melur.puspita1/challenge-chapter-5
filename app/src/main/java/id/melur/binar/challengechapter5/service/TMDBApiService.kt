package id.melur.binar.challengechapter5.service

import id.melur.binar.challengechapter5.model.Movie
import id.melur.binar.challengechapter5.model.MoviePopular
import id.melur.binar.challengechapter5.model.MoviePopularItem
import id.melur.binar.challengechapter5.model.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBApiService {

    @GET("movie/popular")
    fun getMoviePopular(@Query("api_key") key: String) : Call<MoviePopular>

    @GET("movie/{movie_id}")
    fun getDetailMovie(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String
    ): Call<MoviePopularItem>
}