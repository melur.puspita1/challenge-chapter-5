package id.melur.binar.challengechapter5.model

import com.google.gson.annotations.SerializedName
import id.melur.binar.challengechapter5.database.MovieEntity

data class Movie(

    @SerializedName("id")
    var id: Int = 0,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("original_title")
    var originalTitle: String? = null,

    @SerializedName("original_language")
    var originalLanguage: String? = null,

    @SerializedName("adult")
    var adult: Boolean?,

    @SerializedName("poster_path")
    var posterPath: String? = null,

    @SerializedName("backdrop_path")
    var backdropPath: String? = null,

    @SerializedName("release_date")
    var releaseDate: String? = null,

    @SerializedName("overview")
    var overview: String? = null,

    @SerializedName("popularity")
    var popularity: Double = 0.0,

    @SerializedName("video")
    var video: Boolean?,

    @SerializedName("vote_average")
    var voteAverage: Double = 0.0,

    @SerializedName("vote_count")
    var voteCount: Int = 0,
)
//{
//
//    fun toMovieEntity(): MovieEntity =
//        MovieEntity(
//            id = id,
//            title = title,
//            originalTitle = originalTitle,
//            originalLanguage = originalLanguage,
//            adult = adult,
//            posterPath = posterPath,
//            backdropPath = backdropPath,
//            releaseDate = releaseDate,
//            overview = overview,
//            popularity = popularity,
//            video = video,
//            voteAverage = voteAverage,
//            voteCount = voteCount
//        )
//}