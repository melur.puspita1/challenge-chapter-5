package id.melur.binar.challengechapter5

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.melur.binar.challengechapter5.adapter.MovieAdapter
import id.melur.binar.challengechapter5.databinding.FragmentHomeScreenBinding
import id.melur.binar.challengechapter5.helper.UserRepo
import id.melur.binar.challengechapter5.helper.viewModelsFactory
import id.melur.binar.challengechapter5.model.Movie
import id.melur.binar.challengechapter5.model.MoviePopular
import id.melur.binar.challengechapter5.model.MoviePopularItem
import id.melur.binar.challengechapter5.model.MovieResponse
import id.melur.binar.challengechapter5.service.TMDBApiService
import id.melur.binar.challengechapter5.service.TMDBClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeScreen : Fragment() {

    private var _binding: FragmentHomeScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var movieAdapter: MovieAdapter

    private lateinit var sharedPref: SharedPreferences

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val apiService : TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, apiService) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentHomeScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("username", Context.MODE_PRIVATE)

        val username = sharedPref.getString("username", "")
        binding.tvWelcome.text = "Welcome, $username!"
//        viewModel.getAllMovie()
//        getDataFromNetWork()
        getAllMovie()
        initRecyclerView()
        profileButtonOnPressed()
    }

    private fun initRecyclerView() {
        movieAdapter = MovieAdapter { id: Int, movie: MoviePopularItem ->
            val bundle = Bundle()
            bundle.putInt("id", id)
            findNavController().navigate(R.id.action_homeScreen_to_detailScreen, bundle)
        }
        binding.rvData.apply {
            adapter = movieAdapter
//            layoutManager = GridLayoutManager(requireContext(), 2)
            layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun profileButtonOnPressed(){
        binding.btnProfile.setOnClickListener {
            findNavController().navigate(R.id.action_homeScreen_to_profileScreen)
        }
    }

    fun getAllMovie() {
        apiService.getMoviePopular(BuildConfig.API_KEY)
            .enqueue(object : Callback<MoviePopular> {
                // kondisi get data berhasil dari http
                override fun onResponse(
                    call: Call<MoviePopular>,
                    response: Response<MoviePopular>
                ) {
                    // response.issuccessful sama dengan 200
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            movieAdapter.updateData(response.body()!!)
                        }
                    }
                    binding.pbMovie.isVisible = false
                }
                // kondisi get data gagal dari server/http, udh bener2 ga bisa di akses
                override fun onFailure(call: Call<MoviePopular>, t: Throwable) {
                    binding.pbMovie.isVisible = false
                }

            })
    }
}


