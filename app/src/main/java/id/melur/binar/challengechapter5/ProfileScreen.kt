package id.melur.binar.challengechapter5

import android.app.DatePickerDialog
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import id.melur.binar.challengechapter5.database.User
import id.melur.binar.challengechapter5.databinding.FragmentHomeScreenBinding
import id.melur.binar.challengechapter5.databinding.FragmentProfileScreenBinding
import id.melur.binar.challengechapter5.helper.UserRepo
import id.melur.binar.challengechapter5.helper.viewModelsFactory
import id.melur.binar.challengechapter5.service.TMDBApiService
import id.melur.binar.challengechapter5.service.TMDBClient
import java.text.SimpleDateFormat
import java.util.*

class ProfileScreen : Fragment() {

    private var _binding: FragmentProfileScreenBinding? = null
    private val binding get() = _binding!!

    private lateinit var sharedPref: SharedPreferences
    private var dataUsername: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val api: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, api) }

    private val cal = Calendar.getInstance()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentProfileScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = context.getSharedPreferences("username", Context.MODE_PRIVATE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        getData()
        val username = sharedPref.getString("username", "")
        binding.etUsernamee.setText("$username")

        viewModel.coba("$username")

        Handler(Looper.getMainLooper()).postDelayed({
            profileField()
        },200)

        dateButtonClicked()
        logoutButtonOnPressed()
        updateButtonOnPressed()
    }

    private fun getData() {
        dataUsername = sharedPref.getString("username", "")
    }

    private fun updateButtonOnPressed(){
        binding.btnUpdate.setOnClickListener {

            val username = binding.etUsernamee.text.toString()
            val name = binding.etNama.text.toString()
            val date = binding.etTanggal.text.toString()
            val address = binding.etAlamat.text.toString()

            viewModel.updateUser(username, name, date, address)
        }
    }

    private fun profileField(){
        val name = viewModel.name.value.toString()
        val dateOfBirth = viewModel.dateOfBirth.value.toString()
        val address = viewModel.address.value.toString()

        binding.etNama.setText("$name")
        binding.etTanggal.setText("$dateOfBirth")
        binding.etAlamat.setText("$address")
    }

    private fun dateButtonClicked(){
        binding.etTanggal.setOnClickListener {
            createDateDialog().show()
        }
    }

    private fun createDateDialog(): DatePickerDialog {
        return DatePickerDialog(requireContext(),
            dateSetListener,
            // set DatePickerDialog to point to today's date when it loads up
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH),
            cal.get(Calendar.DAY_OF_MONTH))
    }

    // create an OnDateSetListener
    private val dateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val myFormat = "MM/dd/yyyy" // mention the format you need
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            val stringDate = sdf.format(cal.time)
            binding.etTanggal.setText(stringDate)
        }

    private fun logoutButtonOnPressed(){
        binding.btnLogout.setOnClickListener {
            val editor = sharedPref.edit()
            editor.clear()
            editor.apply()
            dataUsername = sharedPref.getString("username", "")
            Toast.makeText(requireContext(), "Logout berhasil", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_profileScreen_to_loginScreen)
        }
    }

    private fun observeData() {
        viewModel.addDataUser.observe(requireActivity()) {
            // buat munculin data dengan manggil data dari database
            viewModel.getData()
        }
        viewModel.user.observe(requireActivity()) {
        }
        viewModel.isLogin.observe(requireActivity()) {
        }
        viewModel.usernamee.observe(requireActivity()) {
        }
        viewModel.name.observe(requireActivity()) {
        }
        viewModel.dateOfBirth.observe(requireActivity()) {
        }
        viewModel.address.observe(requireActivity()) {
        }
    }
}