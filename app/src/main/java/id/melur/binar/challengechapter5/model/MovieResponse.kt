package id.melur.binar.challengechapter5.model

import com.google.gson.annotations.SerializedName

data class MovieResponse(

    @field:SerializedName("results")
    val results: List<Movie>
)