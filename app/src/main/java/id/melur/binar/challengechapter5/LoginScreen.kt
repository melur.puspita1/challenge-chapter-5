package id.melur.binar.challengechapter5

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter5.databinding.FragmentLoginScreenBinding
import id.melur.binar.challengechapter5.helper.UserRepo
import id.melur.binar.challengechapter5.helper.viewModelsFactory
import id.melur.binar.challengechapter5.service.TMDBApiService
import id.melur.binar.challengechapter5.service.TMDBClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginScreen : Fragment() {
    private var _binding: FragmentLoginScreenBinding? = null
    private val binding get() = _binding!!
    private lateinit var sharedPref: SharedPreferences
    private var dataUser: String? = ""

    private val userRepo : UserRepo by lazy { UserRepo(requireContext()) }

    private val api: TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel: ViewModel by viewModelsFactory { ViewModel(userRepo, api) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentLoginScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedPref = requireContext().getSharedPreferences("username", Context.MODE_PRIVATE)
        dataUser = sharedPref.getString("username", "")
        loginButtonOnPressed()
        regisButtonOnPressed()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        sharedPref = context.getSharedPreferences("username", Context.MODE_PRIVATE)
    }

    private fun regisButtonOnPressed() {
        binding.tvRegister.setOnClickListener {
            findNavController().navigate(R.id.action_loginScreen_to_registerScreen)
        }
    }

    private fun loginButtonOnPressed() {
        binding.btnLogin.setOnClickListener {
            val username = binding.etUsername.text.toString()
            val password = binding.etPassword.text.toString()
            if (username != "" && password != "") {
                viewModel.checkRegisteredUser(username, password)
                Handler(Looper.getMainLooper()).postDelayed({
                    // This method will be executed once the timer is over
                    if (viewModel.isLogin.value == true) {
                        Toast.makeText(requireContext(), "Login Berhasil", Toast.LENGTH_SHORT).show()
                        val editor = sharedPref.edit()
                        editor.putString("username", username)
                        editor.apply()
                        findNavController().navigate(R.id.action_loginScreen_to_homeScreen)
                    } else {
                        Toast.makeText(requireContext(), "Email atau Password tidak sesuai", Toast.LENGTH_SHORT).show()
                    }
                },200)
            } else {
                Toast.makeText(requireContext(), "Field tidak boleh kosong", Toast.LENGTH_SHORT).show()
            }
        }
    }
}